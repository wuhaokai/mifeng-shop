package com.mifeng.strategy;

import com.mifeng.constant.AuthConstant;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service(AuthConstant.MEMBER_LOGIN)
public class MemberLoginStrategyImpl implements LoginStrategy{
    @Override
    public UserDetails doLogin( String username,String ip) {
        System.out.println("会员的登录逻辑");
        return null;
    }
}
