package com.mifeng.strategy;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mifeng.constant.AuthConstant;
import com.mifeng.domain.LoginSysUser;
import com.mifeng.ex.AuthException;
import com.mifeng.mapper.LoginSysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.Set;

@Service(AuthConstant.SYS_USER_LOGIN)
public class SysUserLoginStrategyImpl implements LoginStrategy{

    @Autowired
    private LoginSysUserMapper loginSysUserMapper;

    /**
     * 管理员登录的逻辑
     * RBAC
     * 1.通过用户查询数据库
     * 2.查询当前用户的权限信息
     * 3.封装成UserDetails 返回即可
     * @param username
     * @return
     */
    @Override
    public UserDetails doLogin( String username,String ip) {
        System.out.println("管理员的登录逻辑");
        LoginSysUser loginSysUser = loginSysUserMapper.selectOne(new LambdaQueryWrapper<LoginSysUser>()
                .eq(LoginSysUser::getUsername,username)
        );
        if(ObjectUtils.isEmpty(loginSysUser)){
            throw new AuthException("当前用户不存在");
        }
        //处理权限
        Set<String> auths =loginSysUserMapper.selectPermsByUserId(loginSysUser.getUserId());
        if(!CollectionUtils.isEmpty(auths)){
            loginSysUser.setAuths(auths);
        }

        return loginSysUser;
    }
}
