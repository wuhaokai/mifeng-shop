package com.mifeng.strategy;

import org.springframework.security.core.userdetails.UserDetails;

public interface LoginStrategy {

    UserDetails doLogin(String username,String ip);
}
