package com.mifeng.service.imp;

import com.mifeng.constant.AuthConstant;
import com.mifeng.ex.AuthException;
import com.mifeng.factory.LoginStrategyFactory;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private LoginStrategyFactory loginStrategyFactory;

    /**
     * 这里是我们自定义登录的逻辑
     * 1.如何判断登录的来源，并且做对应的登录逻辑
     * 让前端在登录请求中携带一个header
     * loginType:sysUserLogin/memberLogin/xxxxxx
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username)  {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String loginType= request.getHeader(AuthConstant.LOGIN_TYPE);
        if(!StringUtils.hasText(loginType)){
            throw new AuthException("登录类型不匹配，非法登录");
        }

        //拿到登录的ip
        String ip = request.getRemoteAddr();

        //策略模式 对修改关闭 对拓展开放
        return loginStrategyFactory.realDoLogin(loginType,username,ip);
    }
}
