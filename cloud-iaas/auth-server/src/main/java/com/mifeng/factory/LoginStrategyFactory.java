package com.mifeng.factory;

import com.mifeng.ex.AuthException;
import com.mifeng.strategy.LoginStrategy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class LoginStrategyFactory /*implements ApplicationContextAware*/ {

    /**
     * memberLogin, MemberLoginStrategy
     * sysUserLogin,SysUserLoginStrategy
     *
     * @Autowired 在项目启动时 会自动的将bean的名词当做key 对应bean对象当做value 封装到map 中去
     */
    @Autowired
    private Map<String, LoginStrategy> loginStrategyMap= new ConcurrentHashMap<>();

    public UserDetails realDoLogin(String loginType,String username,String ip) throws AuthException {
        LoginStrategy loginStrategy = loginStrategyMap.get(loginType);
        if(ObjectUtils.isEmpty(loginStrategy)){
            throw new AuthException("登录类型不匹配，非法登录");
        }
        return loginStrategy.doLogin(username,ip);
    }

//    /**
//     * 从ioc中取到我想要的bean集合
//     * 填充上面的私有属性
//     * @param applicationContext
//     * @throws BeansException
//     */
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        Map<String, LoginStrategy> strategyMap = applicationContext.getBeansOfType(LoginStrategy.class);
//        strategyMap.forEach((k,v)->{
//            loginStrategyMap.put(k,v);
//        });
//    }
}
