package com.mifeng.config;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mifeng.constant.AuthConstant;
import com.mifeng.ex.AuthException;
import com.mifeng.model.LoginResult;
import com.mifeng.model.Result;
import com.mifeng.service.imp.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.UUID;

@Configuration
public class AuthSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 关于登录的自定义逻辑
     * usernamePasswordAuthenticationFilter
     * DaoAuthenticationProvider
     * userDetailsService # loadUserByUsername
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    /**
     * 自定义网络请求相关的逻辑
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().disable();
        //security 只要告诉他哪一个接口是登录 就可以
        http.formLogin()
                .loginProcessingUrl(AuthConstant.LOGIN_URL)
                  .successHandler(authenticationSuccessHandler())
                  .failureHandler(null);
        //登出
        http.logout()
                .logoutUrl(AuthConstant.LOGOUT_URL)
                .logoutSuccessHandler(logoutSuccessHandler());
        http.authorizeHttpRequests()
                .anyRequest()
                .authenticated();
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 1.生产一个token
     * 2.将token和认证对象存入redis
     * 3.将数据返回给前端
     * @return
     */
    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return (request, response, authentication) -> {
            //生产一个token
            String token = UUID.randomUUID().toString();
            String authenticationStr = JSON.toJSONString(authentication);
            redisTemplate.opsForValue().set(AuthConstant.LOGIN_TOKEN_PREFIX+token,
                    authenticationStr, Duration.ofSeconds(AuthConstant.TOKEN_EXPIRES_TIME));
            //成功以后 将数据 以json 的形式写出去即可
            LoginResult loginResult = new LoginResult();
            loginResult.setAccessToken(token);
            loginResult.setExpireIn(AuthConstant.TOKEN_EXPIRES_TIME);
            loginResult.setType(AuthConstant.BEARER);
            //创建一个对象装换器
            ObjectMapper objectMapper = new ObjectMapper();
            //封装成功的结果
            Result<LoginResult> result = Result.success(loginResult);
            String s = objectMapper.writeValueAsString(result);
            //设置响应的编码
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            PrintWriter writer = response.getWriter();
            writer.write(s);
            writer.flush();
            writer.close();
        };
    }

    /**
     * 处理失败的异常
     * 返回给前端去
     * @return
     */
    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler(){
        return (request, response, exception) -> {
            //设置响应的编码
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            Result<String> result = new Result<>();
            result.setCode(HttpStatus.UNAUTHORIZED.value());
            if(exception instanceof LockedException){
                result.setMsg("账户被锁定，登录失败");
            }else if (exception instanceof BadCredentialsException) {
                result.setMsg("账户或者密码错误，登陆失败！");
            } else if (exception instanceof DisabledException) {
                result.setMsg("账户被禁用，登陆失败！");
            } else if (exception instanceof AccountExpiredException) {
                result.setMsg("账户已过期，登陆失败！");
            } else if (exception instanceof CredentialsExpiredException) {
                result.setMsg("密码已过期，登陆失败！");
            } else if (exception instanceof AuthException) {
                result.setMsg(exception.getMessage());
            } else {
                result.setMsg("登陆失败!");
            }
            ObjectMapper objectMapper = new ObjectMapper();
            String s = objectMapper.writeValueAsString(result);
            PrintWriter writer = response.getWriter();
            writer.write(s);
            writer.flush();
            writer.close();
        };
    }

    /**
     * 将redis的token删除即可
     */
    @Bean
    public LogoutSuccessHandler logoutSuccessHandler(){
        return (request, response, authentication) -> {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            //拿到请求头
            String authorization = request.getHeader(AuthConstant.AUTHORIZATION);
            String token = authorization.replaceFirst(AuthConstant.BEARER, "");
            redisTemplate.delete(AuthConstant.LOGIN_TOKEN_PREFIX+token);
            Result<String> result = Result.success("登出成功");
            ObjectMapper objectMapper = new ObjectMapper();
            String s = objectMapper.writeValueAsString(result);
            PrintWriter writer = response.getWriter();
            writer.write(s);
            writer.flush();
            writer.close();

        };
    }
}
