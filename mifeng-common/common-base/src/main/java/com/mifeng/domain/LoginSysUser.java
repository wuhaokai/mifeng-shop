package com.mifeng.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
    * 系统用户
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_user")
public class LoginSysUser implements Serializable ,UserDetails{

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 用户名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    private String password;



    /**
     * 状态  0：禁用   1：正常
     */
    @TableField(value = "`status`")
    private Integer status;


    /**
     * 用户所在的商城Id
     */
    @TableField(value = "shop_id")
    private Long shopId;

    /**
     * 权限信息
     */
    @TableField(exist = false)
    private Set<String> auths;

    /**
     * 防止token被盗用
     */
    @TableField(exist = false)
    private String ip;


    /**
     * 在auth-server 不需要处理权限
     * 是因为分布式系统中 授权和解析 是分开的
     *
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    /**
     * 在security中保证用户名唯一
     * @return
     */

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.status ==1;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.status ==1;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.status ==1;
    }

    @Override
    public boolean isEnabled() {
        return this.status ==1;
    }

    public void setAuth(Set<String> auths){
        HashSet<String> allAuths = new HashSet<>();
        //处理权限的分割问题
        auths.forEach(auth->{
            if(auth.contains(",")){
                String[] realAuths = auth.split(",");
                for (String realAuth : realAuths) {
                    allAuths.add(realAuth);
                }
            }else{
                allAuths.add(auth);
            }
        });
        this.auths=allAuths;
    }
}