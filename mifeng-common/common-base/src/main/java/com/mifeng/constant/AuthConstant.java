package com.mifeng.constant;


import java.util.Arrays;
import java.util.List;

public interface AuthConstant {

    /**
     * 网关放行的路径
     */
    List<String> GATEWAY_ALLOW_URLS=Arrays.asList("/auth-server/doLogin");

    /**
     * 请求token的key
     */
    String AUTHORIZATION = "Authorization";

    /**
     * token的前缀
     */
    String BEARER = "bearer ";

    /**
     * redis 存储的token前缀
     */
    String LOGIN_TOKEN_PREFIX="login_token";

    /**
     * 登录的路径
     */
    String LOGIN_URL = "/doLogin";

    /**
     * 登出的路径
     */
    String LOGOUT_URL="/doLogout";

    /**
     * 登录用户的类型
     */
    String LOGIN_TYPE="loginType";

    /**
     * 后台管理员登录
     */
    String SYS_USER_LOGIN ="sysUserLogin";

    /**
     * 前台会员登录
     */
    String MEMBER_LOGIN = "memberLogin";

    /**
     * token的过期时间
     */
    Long TOKEN_EXPIRES_TIME= 7200L;

    /**
     * token过期的临界值
     */
    Long TOKEN_EXPIRES_THRESHOLD = 1800L;

    /**
     * 特殊rpc的token前缀
     */
    String MY_RPC_TOKEN_PREFIX = "myRpcToken";
}
