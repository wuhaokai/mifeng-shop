package com.mifeng.config;

import com.mifeng.constant.AuthConstant;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * 直接在common-core 中去写一个 token的传递机制
 * 就不需要在每一个服务中去做了
 * 只要你的服务中心需要发rpc
 * 就需要开启rpc 的功能
 */
@Component
public class FeignTokenInterceptor implements RequestInterceptor {


    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 1.浏览器 ------token -------> A ------token --------> B-------token------>C
     * 2.A ------>B     （在A的项目中 一启动就要发rpc(定时任务)）/mq(rpc)   此时是没有token 的
     * 3.第三方服务------->A
     * /a
     * /b
     * /c
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (!ObjectUtils.isEmpty(requestAttributes)) {
            //这个request 是浏览器的request 里面的token
            HttpServletRequest request = requestAttributes.getRequest();
            String authorization = request.getHeader(AuthConstant.AUTHORIZATION);
            if (StringUtils.hasText(authorization)) {
                requestTemplate.header(AuthConstant.AUTHORIZATION,authorization);
            }
            // 说明要么是第二种 要么是第三种
            // 放行这些特殊情况的api  放行是可以的 但是不安全 每一个人都可以请求我
            // 针对特殊的rpc接口 我们不选择放行 而是携带一个特殊的标记
            String myRpcToken = UUID.randomUUID().toString();
            // 存入redis
            redisTemplate.opsForValue().set(AuthConstant.MY_RPC_TOKEN_PREFIX + ":" + myRpcToken, "");
            requestTemplate.header(AuthConstant.MY_RPC_TOKEN_PREFIX, myRpcToken);
        }
    }
}
