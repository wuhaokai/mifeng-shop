package com.mifeng.constant;

import java.util.Arrays;
import java.util.List;

public interface ResourceConstant {

    String[] RESOURCE_ALLOW_URLS = {
            "/v2/api-docs",  // swagger  druid ...
            "/v3/api-docs",
            "/swagger-resources/configuration/ui",  //用来获取支持的动作
            "/swagger-resources",                   //用来获取api-docs的URI
            "/swagger-resources/configuration/security",//安全选项
            "/webjars/**",
            "/swagger-ui/**",
            "/druid/**",
            "/actuator/**"
    };
    /**
     * 这个是特殊的rpc 接口
     */
    List<String> MY_RPC_URLS=Arrays.asList("/a","/b");

}
