package com.mifeng.utils;

import com.mifeng.constant.ResourceConstant;

import java.util.List;

public class MyAllowPathUtil {

    public static String[] securityAllowPaths() {
        String[] resourceAllowUrls = ResourceConstant.RESOURCE_ALLOW_URLS;
        List<String> myRpcUrls = ResourceConstant.MY_RPC_URLS;
        String[] securityAllowPaths = new String[resourceAllowUrls.length + myRpcUrls.size()];
        for (int i = 0; i < resourceAllowUrls.length; i++) {
            securityAllowPaths[i] = resourceAllowUrls[i];
        }
        for (int i = 0; i < myRpcUrls.size(); i++) {
            securityAllowPaths[resourceAllowUrls.length + i] = myRpcUrls.get(i);
        }
        return securityAllowPaths;
    }
}
