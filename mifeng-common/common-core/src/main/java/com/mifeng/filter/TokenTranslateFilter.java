package com.mifeng.filter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mifeng.constant.AuthConstant;
import com.mifeng.constant.ResourceConstant;
import com.mifeng.domain.LoginSysUser;
import com.mifeng.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class TokenTranslateFilter extends OncePerRequestFilter {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 解析的动作
     * 就是拿token 查redis 转成认证对象
     * 放入security上下文中去
     * -------
     * token 续约
     * 如果token被其他的网站盗取 或者被其他的机器盗取 应该让token失效 如何处理
     * -------
     * 思考：
     * 如何去做多端登录
     * 如何做同端互斥登录
     *
     * 逻辑
     * 我这个过滤器只管token的转换
     * 如果你没有携带token 直接放行 交给security的过滤器来处理
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        //1.拿token
        String authorization = request.getHeader(AuthConstant.AUTHORIZATION);
        if(StringUtils.hasText(authorization)){
            String token = authorization.replaceFirst(AuthConstant.BEARER, "");
            //转换token
            String  authenticationStr = (String) redisTemplate.opsForValue().get(AuthConstant.LOGIN_TOKEN_PREFIX + token);
            //转换这个对象
            UsernamePasswordAuthenticationToken authenticationToken = JSON.parseObject(authenticationStr, UsernamePasswordAuthenticationToken.class);
            //这里需要重新处理权限
            LoginSysUser loginSysUser = JSON.parseObject(authenticationToken.getPrincipal().toString(), LoginSysUser.class);

            // 拿到请求的ip地址
            String requestIp = request.getRemoteAddr();
            if (!requestIp.equals(loginSysUser.getIp())) {
                // 返回一个错误信息
                handlerFailResp(response);
                return;
            }
            // 先做过期判断 用户一直在访问 没有必要过期删除
            Long expireTime = redisTemplate.getExpire(AuthConstant.LOGIN_TOKEN_PREFIX + token, TimeUnit.SECONDS);
            if (expireTime < AuthConstant.TOKEN_EXPIRES_THRESHOLD) {
                redisTemplate.expire(AuthConstant.LOGIN_TOKEN_PREFIX + token, Duration.ofSeconds(AuthConstant.TOKEN_EXPIRES_TIME));
            }
            //拿到权限
            Set<String> auths = loginSysUser.getAuths();
            List<SimpleGrantedAuthority> authorityList = auths.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            //重新创建一个认证对象 因为权限的设置只能通过构造方法来传入

            UsernamePasswordAuthenticationToken realAuthenticationToken = new UsernamePasswordAuthenticationToken(loginSysUser,null,authorityList);

            //放在securityContextHolder
            SecurityContextHolder.getContext().setAuthentication(realAuthenticationToken);
        }
        // 拿请求路径
        String path = request.getRequestURI();
        if (ResourceConstant.MY_RPC_URLS.contains(path)) {
            // 判断有没有带我们的rpcToken
            String myRpcToken = request.getHeader(AuthConstant.MY_RPC_TOKEN_PREFIX);
            // 查询redis有没有
            if (StringUtils.hasText(myRpcToken) && redisTemplate.hasKey(AuthConstant.MY_RPC_TOKEN_PREFIX + ":" + myRpcToken)) {
                // 删掉
                redisTemplate.delete(AuthConstant.MY_RPC_TOKEN_PREFIX + ":" + myRpcToken);
            } else {
                handlerFailResp(response);
                return;
            }
        }
        //如果没有token 我的过滤器不关心
        filterChain.doFilter(request,response);

    }

    private void handlerFailResp(HttpServletResponse response) {
        Result<Object> result = Result.fail(HttpStatus.UNAUTHORIZED.value(), "非法访问");
        ObjectMapper objectMapper = new ObjectMapper();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        String s = null;
        try {
            s = objectMapper.writeValueAsString(result);
            PrintWriter writer = response.getWriter();
            writer.write(s);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
